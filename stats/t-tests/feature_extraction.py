import pandas as pd, numpy as np, scipy.stats as stats

__author__ = 'keo7'

def load_data():
    data = pd.read_csv("/home/keo7/.projects/china_data/data/processed/neg.csv", header=0,
                       delimiter=",", index_col=0)
    return data


def correlate_mz(results, mz_values):
    correlated_results = []
    for index, val in enumerate(mz_values):
        correlated_results.append([val, results[0][index], results[1][index]])
    correlated_results = sorted(correlated_results, key=lambda tup: tup[2])
    return correlated_results

def return_significant_p_vals(outcome, cutoff):
    sig = []
    for x in outcome:
        if x[2] < float(cutoff):
            sig.append(x)
    return sig


def ttests(data, test, cutoff):

    data = data.drop("ID", 1)
    mz_labels = list(data.columns.values)

    d0 = data.loc[data.index.get_loc(test[0]) == True] # PANDAS SUCKS AT INDEXING
    d1 = data.loc[data.index.get_loc(test[1]) == True]

    # Paired t-tests
    results = np.asarray(stats.ttest_ind(d0.values, d1.values, equal_var=False))

    results = correlate_mz(results, mz_labels)


    results = return_significant_p_vals(results, cutoff)
    return results, d0, d1

def create_new_data(results, data, test):
    data = data.loc[test]

    sig_mz = ["ID"]

    for x in results:
        sig_mz.append(x[0])



    data =  data[sig_mz[:]]
    data.to_csv(test[0]+"v"+test[1]+".csv")

    return data


if __name__ == "__main__":
    possible_classes = ["CAN", "CON", "UNK"]
    cutoff = 0.05

    data = load_data()
    results, d0, d1 = ttests(data, possible_classes, cutoff)

    data = create_new_data(results, data, possible_classes)

