import sys, pandas as pd, pylab as pl
from sklearn import preprocessing
from sklearn.decomposition import PCA
from pylab import *


params = {
    'axes.labelsize': 8,
    'text.fontsize': 8,
    'legend.fontsize': 10,
    'xtick.labelsize': 10,
    'ytick.labelsize': 10,
    'legend.scatterpoints' : 1,
}
rcParams.update(params)


def load_data(csvfile):
    data = pd.read_csv(csvfile, index_col=(0,1))

    ylabels = [x for x, _ in data.index]
    labels = [text for _, text in data.index]

    encoder = preprocessing.LabelEncoder().fit(ylabels)

    xdata = data.as_matrix(data.columns)
    ydata = encoder.transform(ylabels)
    target_names = encoder.classes_

    means = [x.mean() for x in xdata]

    return xdata, ydata, target_names, labels, means

def plot_pca(xdata, ydata, target_names, labels, means):
    pca = PCA(
        whiten = True
    )
    X_r = pca.fit(xdata).transform(xdata)

    pl.figure()  # Make a plotting figure
    for i, (c, m, target_name) in enumerate(zip(
            'rbmkycg', 'o^s*v+x', target_names)):
        pl.scatter(X_r[ydata == i, 0], X_r[ydata == i, 1],
                   color=c, marker=m, label=target_name)
        pl.xlabel("PC 1 (% .1f %%)" % (
            pca.explained_variance_ratio_[0] * 100.0))
        pl.ylabel("PC 2 (% .1f %%)" % (
            pca.explained_variance_ratio_[1] * 100.0))

        for n, x, y in zip(
                (ydata == i).nonzero()[0],
                X_r[ydata == i, 0],
                X_r[ydata == i, 1]):
            pl.annotate(
                "Label:"+ str(labels[n]) + "\nMean:"+ str("%.2f" % means[n]),
                xy=(x, y),
                xytext=(5, 5),
                textcoords='offset points',
                color=c,
                fontsize='small',
                ha='left',
                va='top')
    pl.legend()
    pl.savefig("./pca.pdf", type="pdf")
    pl.show()

if __name__ == '__main__':
    try:
        csvfile = sys.argv[1]
    except IndexError:
        print '%s\n\nUsage: %s [--3d] <csv_file>' % (__doc__, sys.argv[0])
        exit(0)

    xdata, ydata, target_names, labels, means = load_data(csvfile)
    plot_pca(xdata, ydata, target_names, labels, means)

